#!/usr/bin/env python

import numpy as np
import math

def sigmoid(w, derivative = False):
    if derivative == True :
        return w * (1 - w)
    else:
        return 1 / (1 + np.exp(-w))


def main():
    input_data = np.array([[0, 0], [0, 1], [1, 0], [1, 1]])
    target_output = np.array([[0, 1, 1, 0]]).T
    w = np.array([[0.1, 0.1]]).T
    counter = 1
    learning_rate = 1
    current = []

    while True:
        hidden_layer = sigmoid(np.dot(input_data, w))
        error = target_output - hidden_layer
        hidden_layer_delta = learning_rate * error * sigmoid(hidden_layer, True)
        w += np.dot(input_data.T, hidden_layer_delta)

        print("\nLoop number: {}".format(counter))
        print("Current weights:")
        for current_w in w:
            print(current_w)

        print("Current hidden layer value:")
        for current_hidden in hidden_layer:
            print(current_hidden)

        if not np.any(current):
            current = error

        status = 0
        if counter > 1:
            for a, b in zip(current, error):
                if math.isclose(a, b, rel_tol=1e-15):
                    continue
                else:
                    status += 1

            if status == 0:
                break
            else:
                current = error

        counter += 1


if __name__ == '__main__':
    main()
