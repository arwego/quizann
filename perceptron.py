#!/usr/bin/env python
# A AND ~B

def dot(values, w):
    score = sum(v * w for v, w in zip(values, w))
    return score


def main():
    w = [0, 0]
    data_set = [((0, 0), 0),
                ((0, 1), 0),
                ((1, 0), 1),
                ((1, 1), 0)]
    step_unit = 0
    learning_rate = 1
    counter = 1

    while True:
        print("\nLoop number: {}".format(counter))
        error_counter = 0
        print("Current weights:")
        for data, target_output in data_set:
            print(w)
            current_result = dot(data, w) > step_unit
            error = target_output - current_result
            if error != 0:
                error_counter += 1
                for index, value in enumerate(data):
                    w[index] += learning_rate * error * value
        if error_counter == 0:
            break
        counter += 1


if __name__ == '__main__':
    main()
