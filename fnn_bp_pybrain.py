#!/usr/bin/env python

import numpy
from pybrain.tools.shortcuts import buildNetwork
from pybrain.supervised.trainers import BackpropTrainer
from pybrain.datasets import SupervisedDataSet


def main():
    n_iteration = 2
    l_rate = 0.3
    momentum = 0.9
    wca = 0.1
    wcb = 0.1
    wdc = 0.1
    wco = 0.1
    wdo = 0.1

    net = buildNetwork(2, 1, 1, bias=True) # input, hidden, output
    print "Network Structure:"
    print net

    ds = SupervisedDataSet(2, 1) # two input, one output
    ds.addSample((1, 0), (1,))  # a = [1, 0]
    ds.addSample((0, 1), (0,))  # b = [0, 1]

    #[  1., 1., 1., 1., 1., 1.,      1., 1., 1.,        1.,       1., 1., 1.    ]
    #     input->hidden0            hidden0->out     bias->out   bias->hidden0
    new_params = numpy.array([wca, wcb, wdc, wdo, wco])
    net._setParameters(new_params)

    trainer = BackpropTrainer(net, ds, learningrate=l_rate, momentum=momentum)
    for i in range(n_iteration):
        print "\nNumber of iteration: {}".format(i + 1)
        trainer.train()
        for c in [connection for connections in net.connections.values() for connection in connections]:
            print("{} -> {} => {}".format(c.inmod.name, c.outmod.name, c.params))


if __name__ == '__main__':
    main()
